const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

// 1. Get all items that are available
// 3. Get all items containing Vitamin A.

const getItems = (key) => {
  if (key === undefined) {
    return items;
  }
  return items.filter((item) => {
    if (!item.contains.includes(key)) {
      return false;
    }
    return true;
  });
};

// 2. Get all items containing only Vitamin C.

const getItemsContainingOnly = (key) => {
  return items.filter((item) => {
    if (!item.contains.includes(key)) {
      return false;
    }
    return !(item.contains.split(",").length > 1);
  });
};

// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }
//
//     and so on for all items and all Vitamins.
const groupBasedOnVitamins = (items) => {
  const splitContains = (item) => item.contains.split(", ");

  return items
    .map(splitContains)
    .flat()
    .reduce((vitaminGroup, vitamin) => {
      vitaminGroup[vitamin] = items
        .filter((item) => splitContains(item).includes(vitamin))
        .map((item) => item.name);
      return vitaminGroup;
    }, {});
};

// 5. Sort items based on number of Vitamins they contain.
const sortBasedOnVitaminCount = (items) => {
  const itemsList = [...items];

  itemsList.sort((firstItem, secondItem) => {
    const lengthOfFirstItem = firstItem.contains.split(",").length;
    const lengthOfSecondItem = secondItem.contains.split(",").length;

    return lengthOfSecondItem - lengthOfFirstItem;
  });

  return itemsList;
};
